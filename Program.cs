﻿using Dotnet_jogo.src.Entities;

namespace Dotnet_jogo
{
    class Program
    {
        public static void Main(string[] args)
        {
            Knight arus = new Knight("Arus", 23, "Edivan");
            Wizard wizard = new Wizard("Jenica", 23, "white wizard");

        

            System.Console.WriteLine(arus.Attack());
            System.Console.WriteLine(wizard.Attack(1));
            System.Console.WriteLine(wizard.Attack(9));
        }
    }
}